# -*- coding: utf-8 -*-
"""
Compulsory Assignment 02 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import modules:
# ==========================================================================
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from perceptron_ext import Perceptron
from adaline_ext import AdalineGD

# ==========================================================================
# Load data without header -> data.shape = (768, 9):
# ==========================================================================
data = pd.read_csv("pima-indians-diabetes.data.csv",
                   names=['pregnant times', 'glucose concentration',
                          'blood pressure', 'triceps thickness',
                          '2-hour insulin', 'body mass',
                          'pedigree', 'age', 'target'])

# ==========================================================================
# Task 1: Inspect and visualise raw data:
# ==========================================================================
# Plot histograms and scatters:
sns.pairplot(data, kind="scatter")
plt.show()

"""
I would remove the following below because the variable values seems to
be unrealistic from the histogram graphs. The training of the remaining
data would be quicker than the complete data with many unreliable values.

'pregnant times': remove the Xs >= 15;
'glucose concentration': remove Xs = 0;
'blood pressure': remove Xs = 0;
'triceps thickness': everything seems ok;
'2-hour insulin': Since many people have 0, then it will not be removed,
even though it seems unreliable;
'body mass': remove Xs = 0;
'pedigree': everything seems ok;
'age': everything seems ok;
'target': everything seems ok.
"""

# Heatmap with the correlation between the
sns.heatmap(data.corr())
plt.show()

"""
We can not identify any apparently high correlation between the variables
from the Heatmap or scatter graphs. However 'glucose concentration' and
'body mass' have the highest correlation between the target we want to
predict, then it could be relevant and faster to try a Machine Learning
algorithm with these to variables to try a faster predict of our target.
"""

# ==========================================================================
# Task 2: Identify 0 values in 'body mass' and 'blood pressure' variables
# and remove the respective rows -> data.shape = (729, 9):
# ==========================================================================
data.drop(index=(data[data['body mass'] == 0].index.tolist() +
                 data[data['blood pressure'] == 0].index.tolist()),
          inplace=True)

# ==========================================================================
# Task 3: First train 400 models (8 subsets of 50, 100, 150, 200, 250, 300,
# 350, 400 patients vs from 1 to 50 epochs) for each Perceptron and
# AdalineGD. Then predict the unseen data for each model. Store the
# results in a (8x50) array:
# ==========================================================================
# Batches and epochs ranges:
batchs, epochs = range(50, 401, 50), range(1, 51)

# List (8) of arrays (batches,) containing the 8 training subsets of Xs & Ys:
train_X = list(map(lambda i: data.drop('target', axis=1)[:i].values, batchs))
train_Y = list(map(lambda i: data['target'][:i].values, batchs))

# Array (329,8) with the unseen Xs & Ys:
test_X = data.drop('target', axis=1).tail(len(data) - 400).values
test_Y = data.target.tail(len(data) - 400).values

# Standardise train_Xis:
train_X = list(map(lambda i, z: (i - np.mean(train_X[z], axis=0))
                   / np.std(train_X[z], axis=0), train_X, range(8)))

# Standardise test_X:
test_X = (test_X - np.mean(test_X, axis=0)) / np.std(test_X, axis=0)

# Convert any target value into 1 and -1 for perceptron:
train_Y = list(map(lambda i: np.where(i == 1, 1, -1), train_Y))
test_Y = np.where(test_Y == 1, 1, -1)

# Train the 400 Perceptron models:
pptron_models = [list(map(lambda i:
                          Perceptron(eta=0.0001,
                                     n_iter=i,
                                     random_state=123).fit(Xi, Yi), epochs))
                 for Xi, Yi in zip(train_X, train_Y)]

# Train the 400 Adaline models:
adaline_models = [list(map(lambda i:
                           AdalineGD(eta=0.0001,
                                     n_iter=i,
                                     random_state=123).fit(X, Y), epochs))
                  for X, Y in zip(train_X, train_Y)]

# Compute the test set classification accuracy for Perceptron:
pptron_pred = []
for a in range(8):
    for b in range(50):
        result = pptron_models[a][b].predict(test_X)
        tt_correct = (result == test_Y).sum()
        idx = tt_correct / len(test_Y)
        pptron_pred.append(idx)
pptron_pred = np.array(pptron_pred).reshape(8, 50)

# Compute the test set classification accuracy for AdalineGD:
adl_pred = []
for a in range(8):
    for b in range(50):
        result = adaline_models[a][b].predict(test_X)
        tt_correct = (result == test_Y).sum()
        idx = tt_correct / len(test_Y)
        adl_pred.append(idx)
adl_pred = np.array(adl_pred).reshape(8, 50)

# ==========================================================================
# Task 4: Plot the heatmaps:
# ==========================================================================
index = ["{} out of 400".format(i) for i in batchs]
columns = ["{} epoch".format(i) for i in epochs]

# Heatmap for the Perceptron's test set classification accuracies:
df_ppt_pred = pd.DataFrame(pptron_pred, index=index, columns=columns)
sns.heatmap(df_ppt_pred)
plt.show()

# Heatmap for the adaline's test set classification accuracies:
df_adl_pred = pd.DataFrame(adl_pred, index=index, columns=columns)
sns.heatmap(df_adl_pred)
plt.show()

# ==========================================================================
# Task 5:
"""Provide the maximum test set classification accuracy for each, the
perceptron classifier and the adaline classifier and information on with
which combination of number training data samples and number of epochs the
best classification accuracy was achieved."""
# ==========================================================================
# Perceptron's maximum test set classification accuracy:
print("The max Perceptron accuracy: {:.2f}%".format(pptron_pred.max()*100))

# Column & row of the Perceptron's maximum classification accuracy:
ppt_maxs = np.where(pptron_pred == pptron_pred.max())

for j, i in zip(ppt_maxs[0], ppt_maxs[1]):
    print("The max accuracy on: " + columns[i] + " and " + index[j] +
          " subset")

# Adaline's maximum test set classification accuracy:
print("The max Adaline accuracy: {:.2f}%".format(adl_pred.max()*100))

# Column & row of the Adaline's maximum classification accuracy:
adl_maxs = np.where(adl_pred == adl_pred.max())

for j, i in zip(adl_maxs[0], adl_maxs[1]):
    print("The max accuracy on: " + columns[i] + " and " + index[j] +
          " subset")
