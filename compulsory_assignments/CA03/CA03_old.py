# -*- coding: utf-8 -*-
"""
Compulsory Assignment 03 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import modules:
# ==========================================================================
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.utils import resample
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import QuantileTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.ensemble import RandomForestClassifier

from mlxtend.feature_selection import ColumnSelector

sns.set(style="whitegrid")
# ==========================================================================
# Load data with "id" as index:
# ==========================================================================
df_train = pd.read_csv("train.csv", index_col="ID")  # (7108, 25)
df_test = pd.read_csv("test.csv", index_col="ID")  # (3828, 24)

# ==========================================================================
# Find missing data:
# ==========================================================================
# Create a list with indexes of missing values:
missing_data_idxs = df_train[df_train.isnull().any(axis=1)].index.tolist()

# ==========================================================================
# Feature Selection:
# ==========================================================================
# Split X and y:
X = df_train.iloc[:, 10:-1]
X = X.values
y = df_train.iloc[:, -1]
y = y.values

# Column Selector:
col_selector = ColumnSelector()

from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
pipe = make_pipeline(StandardScaler(),
                     ColumnSelector(), 
                     KNeighborsClassifier())
pipe.fit(X, y)
pipe.score(X, y)

# ==========================================================================
# Find and delete outliners:
# ==========================================================================
def otl_nonsym_idx(df, column):
    q1 = df[column].quantile(q=0.25)
    q3 = df[column].quantile(q=0.75)
    iqr = q3 - q1
    q0 = q1 - (1.5*iqr)
    q4 = q3 + (1.5*iqr)
    return df[(df[column] < q0)].index.tolist() + \
        df[(df[column] > q4)].index.tolist()

col = ["f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13",
       "f14", "f16", "f18", "f19", "f20", "f21"]

col = df_train.columns

list_drop = []
for i in col:
    list_drop += otl_nonsym_idx(df_train, i)
set_drop = set(list_drop)
print(len(set_drop))

# ==========================================================================
# Drop rows or columns:
# ==========================================================================
categorical_features = ["f17", "f2", "f3", "f18", "f1", "f16"]
df_train.drop(columns=categorical_features, inplace=True)
df_test.drop(columns=categorical_features, inplace=True)

# ==========================================================================
df0 = df_train.groupby("label").size()

# Bootstrap the samples:
df1 = df_train

# Bootstrap classes:
df_majority = df1[df1.label == 1]
df_middle = df1[df1.label == 0]
df_minority = df1[df1.label == 2]

# 2) Oversample minority class
df_majority_oversampled = resample(df_majority, replace=True,
                                   n_samples=20000, random_state=100)

df_minority_oversampled = resample(df_minority, replace=True,
                                   n_samples=20000, random_state=100)

df_middle_oversampled = resample(df_middle, replace=True,
                                 n_samples=20000, random_state=100)

# 3) Combine oversampled minority class with majority class
df1 = pd.concat([df_minority_oversampled, df_middle_oversampled,
                 df_majority_oversampled])

# 4) Display new class counts
df1.label.value_counts()

# ==========================================================================
# Distribution analysis:
# ==========================================================================
# Plot histograms for distribution analysis:
df_train.hist(figsize=(20, 15), bins=25)
plt.show()

# Individualized analysis:
f, ax = plt.subplots(figsize=(20, 7))
sns.distplot(df_train.f24)
plt.show()

# Plot boxplot to visualise outliners:
sns.boxplot(data=df_train.iloc[:, 18])
plt.show()

"""
Continuous features with exponential distribution (QuantileTransformer):
    ["f4", "f5", "f6", "f7", "f8", "f9", "f10", "f14", "f20", "f21", "f24"]

Continuous features with normal distribution (standardize):
    ["f11", "f12", "f13", "f19", "f22"]

Categorical features (normalization):
    ["f1", "f2", "f3"*, "f15", "f16"*, "f17", "f18", "f23"]
"""


# Divide dataframes per type of distribution:
idxs = df_train.index
fe_columns = ["f4", "f5", "f6", "f7", "f8", "f9", "f10", "f14",
              "f20", "f21", "f24"]
df_fe = df_train[fe_columns]

fn_columns = ["f11", "f12", "f13", "f19", "f22"]
df_fn = df_train[fn_columns]

fc_columns = ["f1", "f2", "f3", "f15", "f16", "f17", "f18", "f23"]
df_fc = df_train[fc_columns]


# QuantileTransformer transformation:
qt = QuantileTransformer(n_quantiles=len(df_fe), random_state=100)
arr_fe = qt.fit(df_fe).transform(df_fe)
df_fe_qt = pd.DataFrame(data=arr_fe, index=idxs, columns=fe_columns)

# StandardScaler transformation:
z = StandardScaler()
arr_fn = z.fit(df_fn).transform(df_fn)
df_fn_z = pd.DataFrame(data=arr_fn, index=idxs, columns=fn_columns)

# Normalizer transformation:
n = Normalizer()
arr_fc = n.fit(df_fc).transform(df_fc)
df_fc_n = pd.DataFrame(data=arr_fc, index=idxs, columns=fc_columns)

df = df_fe_qt.join(df_fn_z)
df = df.join(df_fc_n)

# ==========================================================================

# ==========================================================================
# Train Model - Logistic Regression:
# ==========================================================================
# Split X and y:
X = df1.iloc[:, 10:-1]
X = X.values
y = df1.iloc[:, -1]
y = y.values

X_train, X_test, y_train, y_test = \
            train_test_split(X, y, test_size=0.3, random_state=100)

# StandardScaler transformation:
z = StandardScaler()
X_test = z.fit(X_train).transform(X_test)
X_train = z.fit(X_train).transform(X_train)

# Train X and y:
logR = LogisticRegression(C=100000, random_state=100, penalty="l1",
                          solver='liblinear', multi_class='auto')
logR.fit(X_train, y_train)
logR.score(X_test, y_test)

# ==========================================================================
# Train Model - Random Forest:
# ==========================================================================
# Split X and y:
X = df1.iloc[:, :-1]
X = X.values
y = df1.iloc[:, -1]
y = y.values

X_train, X_test, y_train, y_test = \
            train_test_split(X, y, test_size=0.25, random_state=100)

# Train X and y:
rf = RandomForestClassifier(n_estimators=10000, criterion="gini",
                            max_depth=25, n_jobs=-1, random_state=100)
rf.fit(X_train, y_train)
rf.score(X_train, y_train)
rf.score(X_test, y_test)
prediction = rf.predict(df_test)
df_prediction = pd.DataFrame(prediction)
df_prediction.to_csv("pred3.csv", header=True, float_format="%d")
