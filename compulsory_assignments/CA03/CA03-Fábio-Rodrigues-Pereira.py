# -*- coding: utf-8 -*-
"""
Compulsory Assignment 03 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import necessaries libraries:
# ==========================================================================
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.utils import resample
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

from mlxtend.feature_selection import SequentialFeatureSelector as SFS

# ==========================================================================
# Import the databases and pre-training verifications:
# ==========================================================================
df = pd.read_csv("train.csv", index_col="ID")
df_test = pd.read_csv("test.csv", index_col="ID")
print("Train dimension: ", df.shape,
      "\nTest dimension: ", df_test.shape)

# Verify null and duplucated features and classes:
print("Train Data Frame:", 
      "\n# of null features and classes:", df.isnull().any().sum(),
      "\n# of duplicated features and classes:", df.duplicated().sum(),
      "\n\nTest Data Frame:",
      "\n# of duplicated features and classes:", df_test.isnull().any().sum(),
      "\n# of duplicated features and classes:", df_test.duplicated().sum())

# Verify the size of each target class:
print(df.groupby("label").size())

# Verify the histograms for distribution analysis:
df.hist(figsize=(20, 15), bins=10)
plt.show()

# Verify boxplot for outliners:
# Features ["f1", "f2", "f3", "f15", "f17", "f19", "f20", "f21", "f22",
# "f23", "f24"]:
sns.boxplot(data=df[["f1", "f2", "f3", "f15", "f17", "f19", "f20", "f21",
                     "f22", "f23", "f24"]])
plt.show()

# Verify boxplot for outliners:
# Features ["f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13",
# "f14"]:
sns.boxplot(data=df[["f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", 
                     "f13", "f14"] ])
plt.show()

# ==========================================================================
# Scenario 1:
# ==========================================================================

# 1.1:

# Split X and y from df:
X = df.iloc[:, :-1]
X = X.values
y = df.iloc[:, -1]
y = y.values

# Tunning parameter n_estimators:
for n_estimators in range(1, 5):
  score_train = []
  score_test = []

  # Tunning parameter max_depth:
  for max_depth in range(10, 51, 10):
    # Split train and test data frame:
    X_train, X_test, y_train, y_test = \
      train_test_split(X, y, test_size=0.2, random_state=100, stratify=y)

    # Call the model and train:
    rf = RandomForestClassifier(n_estimators=10**n_estimators, 
                                criterion="gini",
                                max_depth=max_depth, 
                                n_jobs=-1, 
                                random_state=100)
    
    rf.fit(X_train, y_train)
        
    score_train.append(rf.score(X_train, y_train))
    score_test.append(rf.score(X_test, y_test))

  sns.lineplot(data=pd.DataFrame(np.array((score_train, score_test)).T,
                                 columns=["score train", "score test"])
              ).set_title("Number of estimator 10**{}".format(n_estimators))
  plt.show()

  print("Raw data frame (df):")
  print("\n Train scores: ", score_train)
  print("\n Test scores: ", score_test)

# ==========================================================================
  
# 1.2:
  
# The best score achieved was (0.6547116736990155) for n_estimators==10**4 and 
# max_depth=50.

# Split X and y from df:
X = df.iloc[:, :-1]
X = X.values
y = df.iloc[:, -1]
y = y.values

# Split train and test data frame:
X_train, X_test, y_train, y_test = \
  train_test_split(X, y, test_size=0.2, random_state=100, stratify=y)

# Call the model and train:
rf = RandomForestClassifier(n_estimators=10**4, 
                            criterion="gini",
                            max_depth=50, 
                            n_jobs=-1, 
                            random_state=100)
    
rf.fit(X_train, y_train)
        
score_train = rf.score(X_train, y_train)
score_test = rf.score(X_test, y_test)

print("Raw data frame (df):")
print("\n Train scores: ", score_train)
print("\n Test scores: ", score_test)

# ==========================================================================

# 1.3:
# I will try the parameters n_estimators==10**3 and max_depth=30, because is 
# computationally less expensive and it does not have much difference from the
# previous test score.

# Split X and y from df:
X = df.iloc[:, :-1]
X = X.values
y = df.iloc[:, -1]
y = y.values

# Split train and test data frame:
X_train, X_test, y_train, y_test = \
  train_test_split(X, y, test_size=0.2, random_state=100, stratify=y)

# Call the model and train:
rf = RandomForestClassifier(n_estimators=10**3, 
                            criterion="gini",
                            max_depth=30, 
                            n_jobs=-1, 
                            random_state=100)
    
rf.fit(X_train, y_train)
        
score_train = rf.score(X_train, y_train)
score_test = rf.score(X_test, y_test)

print("Raw data frame (df):")
print("\n Train scores: ", score_train)
print("\n Test scores: ", score_test)

# ==========================================================================
# Scenario 2:
# ==========================================================================

def quantile_technique(df, column):
    q1 = df[column].quantile(q=0.25)
    q3 = df[column].quantile(q=0.75)
    iqr = q3 - q1
    q0 = q1 - (1.5*iqr)
    q4 = q3 + (1.5*iqr)
    return df[(df[column] < q0)].index.tolist() + \
        df[(df[column] > q4)].index.tolist()

# Take the indexes of the outliers:
outliers_qtl = []
for i in df.columns:
    outliers_qtl += quantile_technique(df, i)
outliers_qtl = set(outliers_qtl)

print(len(outliers_qtl))

# Not feasible

# ==========================================================================
# Scenario 3:
# ==========================================================================

# Get indexes of outliers with values above or equal 1.000:
outliers_a1000 = []
for col in df.columns:
  outliers_a1000 += df[(df[col]>=1000)].index.tolist()
outliers_a1000 = set(outliers_a1000)
print(len(outliers_a1000))

# Create a new data frame without the dropped indexes: 
df1a = df.drop(index=outliers_a1000)
print(df1a.shape)

# Split X and y from df:
X = df1a.iloc[:, :-1]
X = X.values
y = df1a.iloc[:, -1]
y = y.values

# Split train and test data frame:
X_train, X_test, y_train, y_test = \
  train_test_split(X, y, test_size=0.2, random_state=100,
                   stratify=y)

# Call the model and train:
rf = RandomForestClassifier(n_estimators=10**3, 
                            criterion="gini",
                            max_depth=30, 
                            n_jobs=-1, 
                            random_state=100)

rf.fit(X_train, y_train)

score_train = rf.score(X_train, y_train)
score_test = rf.score(X_test, y_test)

print("Raw data frame (df):")
print("\n Train scores: ", score_train)
print("\n Test scores: ", score_test)

# Conclusion: Test score less than scenario 1.3, then there is no improvement.

# ==========================================================================
# Scenario 4:
# ==========================================================================

# 1a) Devide the samples according to the target class:
df2b_majority = df[df.label == 1]
df2b_middle = df[df.label == 0]
df2b_minority = df[df.label == 2]

# 2a) Oversample the data frames:
df2b_majority_oversampled = resample(df2b_majority, replace=True,
                                     n_samples=20000, random_state=100)

df2b_minority_oversampled = resample(df2b_minority, replace=True,
                                     n_samples=20000, random_state=100)

df2b_middle_oversampled = resample(df2b_middle, replace=True,
                                   n_samples=20000, random_state=100)

# 3a) Combine oversampled classes:
df2b = pd.concat([df2b_minority_oversampled, df2b_middle_oversampled,
                 df2b_majority_oversampled])

# 4) Display new class counts:
print("df2b:\n", df2b.label.value_counts())


# Split X and y from df:
X = df2b.iloc[:, :-1]
X = X.values
y = df2b.iloc[:, -1]
y = y.values

# Split train and test data frame:
X_train, X_test, y_train, y_test = \
  train_test_split(X, y, test_size=0.2, random_state=100,
                   stratify=y)

# Call the model and train:
rf = RandomForestClassifier(n_estimators=10**4, 
                            criterion="gini",
                            max_depth=50, 
                            n_jobs=-1, 
                            random_state=100)

rf.fit(X_train, y_train)

score_train = rf.score(X_train, y_train)
score_test = rf.score(X_test, y_test)

print("Raw data frame (df):")
print("\n Train scores: ", score_train)
print("\n Test scores: ", score_test)

"""
**Conclusion:** The train and test score achieved in this scenario was very 
high, but it was expected because of the oversample. Oversample leads to 
higher variance, then less accuracy. **Kaggle score was 0,69226, slightly 
higher than the scenario 1.2 or 1.3 whithout any feature analyze.**
"""

# ==========================================================================
# Scenario 5:
# ==========================================================================

# Check correlation between variables:
sns.heatmap(data=df.corr())
plt.show()

# Split X and y from df:
X = df.iloc[:, :-1]
X = X.values
y = df.iloc[:, -1]
y = y.values

# Feature importance with mlxtend
from mlxtend.evaluate import feature_importance_permutation

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, stratify=y, random_state=100)

rfs = RandomForestClassifier(n_estimators=10**3, criterion="gini", 
                             max_depth=30, n_jobs=-1,
                             random_state=100)

rfs.fit(X_train, y_train)
print(rfs.score(X_test, y_test))
 
# Compute importances from training and test data
imp_vals, imp_all = feature_importance_permutation(
    predict_method=rfs.predict,
    X=X_test,
    y=y_test,
    metric='accuracy',
    num_rounds=100,
    seed=100)

# Collect importances of each feature
print('\n', imp_vals)
print('\n', imp_all)

# Visualization of Feature permutation importance (mlxtend):
indices = np.argsort(imp_vals)[::-1]
plt.figure()
plt.title("Random Forest feature importance via permutation importance")
plt.bar(range(X.shape[1]), imp_vals[indices])
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.ylim([0, 0.5])
plt.show()


# 5.1)

# Worst features: (label above + 1) = ["f14", "f13", "f8", "f11"]
# Split X and y from df:
drop_cols_fip = ["f14", "f13", "f8", "f11", "f6", "f10", "f2"]
df_fip = df.drop(columns=drop_cols_fip)
X1 = df_fip.iloc[:, :-1]
X1 = X1.values
y1 = df_fip.iloc[:, -1]
y1 = y1.values

X1_train, X1_test, y1_train, y1_test = train_test_split(
    X1, y1, test_size=0.2, stratify=y1, random_state=100)

rf_fip = RandomForestClassifier(n_estimators=10**3, criterion="gini", 
                                max_depth=30, n_jobs=-1, random_state=100)

rf_fip.fit(X1_train, y1_train)

print('Training accuracy:', rf_fip.score(X1_train, y1_train))
print('Test accuracy:', rf_fip.score(X1_test, y1_test))


# 5.2)

# Split X and y from df:
drop_cols_fip = ["f14", "f13", "f8", "f11", "f6", "f10", "f2"]
df_fip = df.drop(columns=drop_cols_fip)

# 1a) Devide the samples according to the target class:
df2b_majority = df_fip[df_fip.label == 1]
df2b_middle = df_fip[df_fip.label == 0]
df2b_minority = df_fip[df_fip.label == 2]

# 2a) Oversample the data frames:
df2b_majority_oversampled = resample(df2b_majority, replace=True,
                                     n_samples=20000, random_state=100)

df2b_minority_oversampled = resample(df2b_minority, replace=True,
                                     n_samples=20000, random_state=100)

df2b_middle_oversampled = resample(df2b_middle, replace=True,
                                   n_samples=20000, random_state=100)

# 3a) Combine oversampled classes:
df2b = pd.concat([df2b_minority_oversampled, df2b_middle_oversampled,
                 df2b_majority_oversampled])

# 4) Display new class counts:
print("df2b:\n", df2b.label.value_counts())
X9 = df2b.iloc[:, :-1]
X9 = X9.values
y9 = df2b.iloc[:, -1]
y9 = y9.values

X9_train, X9_test, y9_train, y9_test = train_test_split(
    X9, y9, test_size=0.2, stratify=y9, random_state=100)

rf_fip2 = RandomForestClassifier(n_estimators=10**3, criterion="gini", 
                                max_depth=30, n_jobs=-1, random_state=100)

rf_fip2.fit(X9_train, y9_train)

print('Training accuracy:', rf_fip2.score(X9_train, y9_train))
print('Test accuracy:', rf_fip2.score(X9_test, y9_test))

"""
**Conclusion:** I combined scenario approaches 1.3: n_estimators=10000 and 
max_depth=30; with 5.1: feature selection by permutation (mlxtend); with 4: 
oversampled and balanced target classes. **Kaggle score: 0.70689. 
    
Slightly improvement and I got the best accuracy here.**
"""
