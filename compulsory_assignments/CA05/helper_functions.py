# -*- coding: utf-8 -*-
"""
Helper functions for the Compulsory Assignment 05 for the course 'DAT200:
Applied Machine Learn' at the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# =======================================================================
# Import necessaries libraries:
# =======================================================================
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.model_selection as skl_model_selection


# =======================================================================
# Helper functions:
# =======================================================================


# Extended multiplicative signal correction
def EMSC(X, reference, degree):
    # Create polynomials up to chosen degree
    poly = []
    pvar = [1]

    for i in range(degree):
        poly.append(np.polyval(pvar, np.linspace(-1, 1, len(reference))))
        pvar.append(0)

    # Reference spectrum and polynomials
    emsc_basis = np.vstack([reference, np.vstack(poly)])

    # Estimate EMSC parameters
    (params, _, _, _) = np.linalg.lstsq(emsc_basis.T, X.T, rcond=None)

    # Correct and return
    return (X - params[1:, :].T @ emsc_basis[1:, :]) / params[:1, :].T


def save_csv(y_hat, path="y_hat"):
    csv = np.vstack(
        [y_hat.flatten(),
         [str(i) for i in range(len(y_hat))]]
    ).T

    pd.DataFrame(
        csv,
        columns=["label", "Id"]).to_csv(
        path,  # float_format="%d",
        index=False
    )


def evaluate(X_fit, y_true, pipeline, search_space, cv_num=5, groups=None):
    cv_inner = skl_model_selection.GroupKFold(
        n_splits=cv_num
    )

    gs = skl_model_selection.GridSearchCV(
        estimator=pipeline,
        param_grid=search_space,
        scoring='neg_root_mean_squared_error',
        cv=cv_inner,
        n_jobs=-1
    )

    gs.fit(X=X_fit, y=y_true, groups=groups)

    print(
        '\nRMSE best score:', gs.best_score_,
        # '\nRMSE score mean:', np.mean(gs.scorer_),
        # '\nRMSE score std:', np.std(gs.scorer_),
        '\nBest Parameters', gs.best_params_
    )

    return gs


def diagnose(y_true, y_hat):
    residuals = y_hat - y_true

    # Diagnose plot 1 (y_true vs y_hat):
    plt.scatter(x=y_true, y=y_hat, c='steelblue', edgecolor='white', s=70)
    plt.xlabel('y_true: Vector of true targets')
    plt.ylabel('y_hat: Vector of predictions')
    plt.show()

    # Diagnose plot 2 (min&max(y_true) vs residuals):
    plt.scatter(
        x=y_true,
        y=residuals,
        c='limegreen',
        marker='s',
        edgecolor='white',
        label='Train data residuals plot'
    )
    plt.xlabel('Predicted values')
    plt.ylabel('Residuals')
    plt.legend(loc='upper left')
    plt.hlines(y=0,
               xmin=min(y_true) - 1,
               xmax=max(y_true) + 1,
               colors='black',
               linestyle='--',
               lw=3)
    plt.show()

    # Diagnose plot 3 (residual's histogram):
    plt.hist(residuals)
    plt.xlabel('Residual values')
    plt.ylabel('Frequency')
    plt.vlines(x=0,
               ymin=0,
               ymax=1000,
               colors='black',
               linestyle='--',
               lw=3)
    plt.show()


# Validation Curve:
def validation_curve_plot(train_scores, test_scores, param_range):
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    plt.title("Validation Curve:")
    plt.xlabel("Parameter")
    plt.ylabel("Score")

    plt.semilogx(param_range,
                 train_scores_mean,
                 label="Training score",
                 color="darkorange",
                 lw=3
                 )

    plt.fill_between(param_range,
                     train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std,
                     alpha=0.2,
                     color="darkorange",
                     lw=3
                     )

    plt.semilogx(param_range,
                 test_scores_mean,
                 label="Cross-validation score",
                 color="navy",
                 lw=3
                 )

    plt.fill_between(param_range,
                     test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std,
                     alpha=0.2,
                     color="navy",
                     lw=3
                     )

    plt.legend(loc="best")
    plt.show()


def compact(mtx, grp):
    idx = np.unique(grp, return_index=True)[1]
    count = np.unique(grp, return_counts=True)[1]
    cpt = np.zeros((len(count), mtx.shape[1]))

    for a in range(len(count)):
        cpt[a, :] = np.mean(mtx[idx[a]:idx[a] + count[a]], axis=0)

    return cpt
