# -*- coding: utf-8 -*-
"""
Compulsory Assignment 05 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# =======================================================================
# Import necessaries libraries:
# =======================================================================
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import sklearn.cross_decomposition as skl_cross_decomposition
import sklearn.feature_selection as skl_feature_selection
import sklearn.model_selection as skl_model_selection
import sklearn.linear_model as skl_linear_model
import sklearn.pipeline as skl_pipeline
import sklearn.metrics as skl_metrics

import sklearn.decomposition as skl_decomposition
import sklearn.ensemble as skl_ensemble
# import sklearn.compose as skl_compose
import sklearn.preprocessing as skl_preprocessing

import sklearn.ensemble as skl_ensemble

import compulsory_assignments.CA05.helper_functions as hf

# =======================================================================
# Pre-processing:
# =======================================================================
# Read data
data = pd.read_pickle("compulsory_assignments/CA05/train.pkl.zip")
data_test = pd.read_pickle("compulsory_assignments/CA05/test.pkl.zip")

# Pre-processing:
# 1) Cut from 500 cm-1 to 3100cm-1 in shifts:
X = data["RamanCal"][:, 536:3136]  # cut matrix
ref = X[1343, :]

X_test = data_test["RamanVal"][:, 536:3136]

# 2) EMSC Corrections:
X_fixed = hf.EMSC(
    X=X,
    reference=ref,
    degree=7
)

X_test_fixed = hf.EMSC(
    X=X_test,
    reference=ref,
    degree=7
)

# 3) Get groups and targets:
groups = data["repCal"][:, np.newaxis]  # vector groups
y_true = data["IodineCal"]  # vector y_true

# 8) Release memory:
del (data, data_test, ref, X, X_test)

# =======================================================================
# Pipeline with compression step and regression method:
# =======================================================================
# Combine multiple pre-processing:
reduce_dim = skl_pipeline.FeatureUnion(
    [
        ('SelectKBest', skl_feature_selection.SelectKBest())
    ]
)

# Create a pipeline:
pipe = skl_pipeline.Pipeline(
    [
        ('reduce_dim', reduce_dim),
        ('regression1', skl_linear_model.Ridge(random_state=100)),
    ]
)

# Create space of candidate values:
search_space = [
    {
        'reduce_dim__SelectKBest__score_func':
            [
                # skl_feature_selection.f_regression,
                skl_feature_selection.mutual_info_regression
            ],
        'reduce_dim__SelectKBest__k':
            [
                700, 800, 900, 1000
                # 2, 50, 100, 500, 800, 1300
            ],
        'regression1__alpha':
            [
                # 0.1, 0.5, 1, 5, 10, 50, 100, 500, 1000, 10000
                12000000, 13000000, 14000000
            ]
    }
]

clf = hf.evaluate(
    X_fit=X_fixed,
    y_true=y_true.flatten(),
    pipeline=pipe,
    search_space=search_space,
    cv_num=5,
    groups=groups
)

# Save csv:
hf.save_csv(
    clf.best_estimator_.predict(X_test_fixed),
    path="compulsory_assignments/CA05/y_hat"
)

"""
RMSE score: -1.239812457199587 
Best Parameters {'reduce_dim__SelectKBest__k': 500, 
'reduce_dim__SelectKBest__score_func': 
<function mutual_info_regression at 0x7f36be966050>, 
'regression1__alpha': 10000}
"""
# **** Score RMSE at kaggle: 1.28973 ****

'''
RMSE score: -1.2271399162080838 
Best Parameters {'reduce_dim__SelectKBest__k': 450, 
'reduce_dim__SelectKBest__score_func': 
<function mutual_info_regression at 0x7f20f6bf7050>, 
'regression1__alpha': 100000}
'''
# **** Score RMSE at kaggle: 1.32020 ****

'''
RMSE score: -1.1436464240637292 
Best Parameters {'reduce_dim__SelectKBest__k': 500, 
'reduce_dim__SelectKBest__score_func': 
<function mutual_info_regression at 0x7f20f6bf7050>, 
'regression1__alpha': 10000000}
'''
# **** Score RMSE at kaggle: 1.23687 ****

'''
RMSE score: -1.1011687231225094 
 Best Parameters {'reduce_dim__SelectKBest__k': 1000, 
 'reduce_dim__SelectKBest__score_func': 
 <function mutual_info_regression at 0x7f20f6bf7050>, 
 'regression1__alpha': 14000000}
'''
# **** Score RMSE at kaggle:  1.27737****

# log(x) transformation:
'''
RMSE score: -1.1068927250713756 
Best Parameters:
{'reduce_dim__SelectKBest__k': 1300, 
'reduce_dim__SelectKBest__score_func': 
<function mutual_info_regression at 0x7f7715b57050>, 
'regression1__alpha': 0.5}
'''
# **** Score RMSE at kaggle: 1.36288 ****

# Release memory:
del (clf, pipe, search_space)

# =======================================================================
# Pre-processed data +  Group CV + GridSearchCV:
# numpy[l, c]
# =======================================================================
sns.boxplot(data=y_true)
plt.show()

# Identify the outliers:
outliers0 = np.where(y_true <= 20)[0][:, np.newaxis]
outliers1 = np.where(y_true >= 30)[0][:, np.newaxis]
outliers = np.vstack([outliers0, outliers1])

# Delete outliers:
X_inliers = np.delete(X_fixed, outliers, axis=0)
y_inliers = np.delete(y_true, outliers, axis=0)
groups_inliers = np.delete(groups, outliers, axis=0)
del (outliers0, outliers1, outliers)

# Filtering the spectra outliers:
for i in range(X_inliers.shape[1]):
    X_inliers[:, i] = np.where(
        X_inliers[:, i] <= np.quantile(X_inliers[:, i], 0.05),
        np.mean(X_inliers[:, i]),  # for true
        X_inliers[:, i]  # for false
    )
    X_inliers[:, i] = np.where(
        X_inliers[:, i] >= np.quantile(X_inliers[:, i], 0.95),
        np.mean(X_inliers[:, i]),  # for true
        X_inliers[:, i]  # for false
    )

# Remove noisy features:
First100 = X_inliers[:, :10]
sns.boxplot(data=First100)
plt.show()

# Plot spectra lines:
plt.plot(X_inliers.T)
plt.show()

# Validation Curve of 2 model: - Models evaluation:
X_fit, y_fit = skl_cross_decomposition.PLSRegression(
    n_components=10).fit_transform(X_inliers, y_inliers)

# Create search spaces:
RF_pipe = skl_pipeline.Pipeline(
    [('RF', skl_ensemble.RandomForestRegressor(n_estimators=100,
                                               n_jobs=-1))])
RF_param_range = [50]
RF_search_space = [{'RF__max_depth': RF_param_range}]

Ridge_pipe = skl_pipeline.Pipeline(
    [('Ridge', skl_linear_model.Ridge(random_state=100))])

Ridge_param_range = [15, 30]
Ridge_search_space = [{'Ridge__alpha': Ridge_param_range}]

PLS_pipe = skl_pipeline.Pipeline(
    [('PLS', skl_cross_decomposition.PLSRegression())])

PLS_param_range = [5, 10]
PLS_search_space = [{'PLS__n_components': PLS_param_range}]

# Release memory:
del (First100, X_fixed, groups, i, y_true)

# Pipelines and parameter_search_spaces combined:
pipe = (RF_pipe, Ridge_pipe)
param_range = (RF_param_range, Ridge_param_range)
search_space = (RF_search_space, Ridge_search_space)

(train_scores, test_scores) = skl_model_selection.validation_curve(
    estimator=RF_pipe,
    X=np.log(X_inliers),
    y=np.sqrt(y_inliers.flatten()),
    groups=groups_inliers,
    param_name="RF__max_depth",  #  "PLS__n_components",
    param_range=RF_param_range,
    scoring='neg_root_mean_squared_error',
    cv=10,
    n_jobs=-1
)

print('\nTrain score mean:', np.mean(train_scores),
      '\nTest score mean:', np.mean(test_scores),
      '\nDifference:', np.mean(train_scores) - np.mean(test_scores), '\n')

gs = hf.evaluate(
    X_fit=X_fit,
    y_true=y_inliers.flatten(),
    pipeline=RF_pipe,
    search_space=RF_search_space,
    cv_num=10,
    groups=groups_inliers
)

# Diagnostic plot:
y_hat = gs.best_estimator_.predict(X_fit)
hf.diagnose(y_inliers, y_hat[:, np.newaxis])

pred_fit = skl_cross_decomposition.PLSRegression(
    n_components=10).fit(X_inliers, y_inliers).transform(X_test_fixed)

# Save CSV:
hf.save_csv(
    gs.best_estimator_.predict(pred_fit),
    path="compulsory_assignments/CA05/RF_y_hat"
)
