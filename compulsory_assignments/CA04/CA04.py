# -*- coding: utf-8 -*-
"""
Compulsory Assignment 04 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""
__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# =======================================================================
# Import necessaries libraries:
# =======================================================================
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import balanced_accuracy_score, confusion_matrix
from sklearn.metrics import recall_score, f1_score, precision_score
from sklearn.utils import resample
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import KernelPCA
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
# ======================================================================
# Helper Functions:
# ======================================================================


def evaluate(df_scenarios, clf):
    for e, df in df_scenarios:
        print('\n', e)

        X_train, X_test, y_train, y_test = \
            train_test_split(df.iloc[:, :-1],
                             df.iloc[:, -1],
                             test_size=0.20,
                             stratify=df.iloc[:, -1],
                             random_state=1)

        clf.fit(X_train, y_train)

        y_pred = clf.best_estimator_.predict(X_test)
        confusion_matrix_plot(y_test, y_pred)

        bal_acc = balanced_accuracy_score(y_test, y_pred)
        prec_acc = precision_score(y_test, y_pred, average="macro")
        rc_acc = recall_score(y_test, y_pred, average="macro")
        f1_acc = f1_score(y_test, y_pred, average="macro")

        print("\nBest Score: ", clf.best_score_,
              "\n",
              "\nBalanced Score: ", bal_acc,
              "\n",
              "\nPrecision Score: ", prec_acc,
              "\n",
              "\nRecall Score: ", rc_acc,
              "\n",
              "\nF1 Score: ", f1_acc,
              "\n",
              "\nBest Parameters: ", clf.best_params_)


def confusion_matrix_plot(y_true, y_pred):
    conf_mat = confusion_matrix(y_true=y_true, y_pred=y_pred)
    print('Confusion matrix:\n', conf_mat)

    labels = ['Class{}'.format(i+1) for i in range(len(conf_mat))]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(conf_mat, cmap=plt.cm.Blues)
    fig.colorbar(cax)
    ax.set_xticklabels([''] + labels)
    ax.set_yticklabels([''] + labels)
    plt.xlabel('Predicted')
    plt.ylabel('Expected')
    plt.show()


def save_csv(df_t, classifier, csv_name):
    df_prediction = pd.DataFrame(
        classifier.predict(df_t.values), columns=["label"])

    df_prediction = pd.concat(
        [df_prediction, pd.DataFrame(
            [i for i in range(len(df_prediction))], columns=["id"])], axis=1)

    df_prediction.to_csv(csv_name,
                         float_format="%d",
                         columns=["label", "id"],
                         index=False)


# =======================================================================
# Read train and test datasets as dataframe:
# =======================================================================
df = pd.read_csv("compulsory_assignments/CA04/glassTrain.csv",
                 index_col="Unnamed: 0")

df_test = pd.read_csv("compulsory_assignments/CA04/glassTest.csv",
                      index_col="Unnamed: 0")

# =======================================================================
# Check raw data:
# =======================================================================
# Verify data frame dimensions:
print("Train dimension: ", df.shape,
      "\nTest dimension: ", df_test.shape)

# Verify null and duplicated samples:
print("Train Data Frame:", 
      "\n# of null/missing values:", df.isnull().any().sum(),
      "\n# of duplicated samples:", df.duplicated().sum(),
      "\n\nTest Data Frame:",
      "\n# of null/missing values:", df_test.isnull().any().sum(),
      "\n# of duplicated samples:", df_test.duplicated().sum())

# Verify targets sizes:
print(df.groupby("type").size())

# Check correlations:
fig, ax = plt.subplots(figsize=(8, 8))
sns.heatmap(df.corr(), annot=True, linewidths=.5, ax=ax)
plt.show()

# Check distributions:
df.hist(figsize=(10,10), bins=50)
plt.show()

# Check outliers:
fig, ax = plt.subplots(figsize=(5,5))
sns.boxplot(data=df[["RI", "Na", "Ca", "Mg", "Al", "K", "Ba", "Fe", "type"]], 
            ax=ax)
plt.show()

# For feature Si:
fig, ax = plt.subplots(figsize=(5,5))
sns.boxplot(data=df.Si, ax=ax)
plt.show()
    
# Descriptive Statistics:
print(df.describe())

# =======================================================================
# Create dataframes with different scenarios:
# df columns: # ["RI", "Fe", "Ca", "Na", "Mg", "Al", "Si", "K", "Ba"]
# =======================================================================
# Drop features with low negative correlations with the target:
df2 = df.drop(["RI", "Fe", "Ca"], axis=1)

# Drop features with many zero values:
df3 = df.drop(["Fe", "Ba"], axis=1)

# Resemble data according to the target classes and balance it:
df4 = pd.DataFrame()

for i in [1, 2, 3, 5, 6, 7]:
    resambled_df = resample(df[df.type == i],
                            replace=True,
                            n_samples=30,
                            random_state=1)

    df4 = pd.concat([df4, resambled_df])

# Scenarios n list:
df_scenarios = [
    ("Scenario 1 - Without any features modification",
     df),
    ("Scenario 2 - Without low and negative correlation features",
     df2),
    ("Scenario 3 - Without many zero value features",
     df3),
    ("Scenario 4 - Resembled and balanced target classes",
     df4)]

# =======================================================================
# SVC + GridSearchCV:
# =======================================================================
# C regularisation and gamma on (0.0001:1000:10**i)
# random state on range(1, 160, 20)
param_grid_svc = [
    {"svc__C": [10**i for i in range(-4, 4)],
     "svc__kernel": ["linear"],
     "svc__class_weight": ["balanced", None],
     "svc__random_state": [i for i in range(1, 160, 20)]
     },
    {"svc__C": [10**i for i in range(-4, 4)],
     "svc__gamma": [10**i for i in range(-4, 4)],
     "svc__kernel": ["rbf"],
     "svc__class_weight": ["balanced", None],
     "svc__random_state": [i for i in range(1, 160, 20)]
     }]

gs = GridSearchCV(estimator=make_pipeline(StandardScaler(),
                                          SVC(random_state=1)),
                  param_grid=param_grid_svc,
                  scoring="accuracy",
                  cv=4,
                  n_jobs=-1
                  )

evaluate(df_scenarios, clf=gs)

# =======================================================================
# SVC + Feature Dimensions + GridSearchCV:
# =======================================================================
# Create a combined preprocessing object:
preprocess = FeatureUnion(
    [('kernelpca', KernelPCA(kernel="rbf"))
     #("kbest", SelectKBest())
     ])

# Create a pipeline
pipe = Pipeline(
    [('preprocess', preprocess),
     ('classifier', SVC(kernel='rbf',
                        random_state=1))
     ])

# Create space of candidate values:
search_space = [
    {'preprocess__kernelpca__n_components': [3, 4, 5, 6, 7],
     'preprocess__kernelpca__gamma': np.linspace(1, 15, 5),
     #'preprocess__kbest__k': [3, 4, 5, 6],

     'classifier__C': np.linspace(10, 150, 5),
     'classifier__gamma': np.linspace(0.1, 2, 5),
     'classifier__class_weight': ["balanced", None]
     }]

# Create grid search:
clf = GridSearchCV(pipe,
                   search_space,
                   cv=2,
                   n_jobs=-1)

evaluate(df_scenarios, clf=clf)

# =======================================================================
# RF + GridSearchCV:
# =======================================================================
# Tuning n_estimations, max_depth, class_weight:
param_grid_rf = [
    {"n_estimators": [10**i for i in range(1, 4)],
     "max_depth": [i for i in range(5, 26, 5)],
     "class_weight": ["balanced", None]
     }]

gs = GridSearchCV(estimator=RandomForestClassifier(random_state=1,
                                                   n_jobs=-1),
                  param_grid=param_grid_rf,
                  scoring="accuracy",
                  cv=4,
                  n_jobs=-1
                  )

evaluate(df_scenarios, clf=gs)

# =======================================================================
# Best models on kaggle:
# =======================================================================
# Research 3, scenario 4:
rf = RandomForestClassifier(n_estimators=1000,
                            max_depth=10,
                            class_weight="balanced",
                            n_jobs=-1,
                            random_state=100)
rf.fit(df4.iloc[:, :-1].values, df4.iloc[:, -1].values)
save_csv(df_test, rf, "compulsory_assignments/CA04/research3_scenario4.csv")
# 0.68571 on kaggle

# Research 3, scenario 3:
rf = RandomForestClassifier(n_estimators=100,
                            max_depth=10,
                            class_weight="balanced",
                            n_jobs=-1,
                            random_state=100)
rf.fit(df3.iloc[:, :-1].values, df3.iloc[:, -1].values)
df_test_sn3 = df_test.drop(["Fe", "Ba"], axis=1)
save_csv(df_test_sn3, rf,
         "compulsory_assignments/CA04/research3_scenario3.csv")
# 0.74285 on kaggle

# Research 3, scenario 2:
rf = RandomForestClassifier(n_estimators=100,
                            max_depth=10,
                            class_weight="balanced",
                            n_jobs=-1,
                            random_state=100)
rf.fit(df2.iloc[:, :-1].values, df2.iloc[:, -1].values)
df_test_sn2 = df_test.drop(["RI", "Fe", "Ca"], axis=1)
save_csv(df_test_sn2, rf,
         "compulsory_assignments/CA04/research3_scenario2.csv")
# 0.77142 on kaggle

# Research 2, scenario 4:
svm = make_pipeline(
    KernelPCA(n_components=5,
              gamma=1,
              kernel="rbf"),
    SVC(C=45,
        gamma=1.525,
        kernel='rbf',
        class_weight="balanced",
        random_state=100))

svm.fit(df4.iloc[:, :-1].values, df4.iloc[:, -1].values)
save_csv(df_test, svm, "compulsory_assignments/CA04/research2_scenario4.csv")
# 0.600 on kaggle

# Research 2, scenario 3:
svm = make_pipeline(
    KernelPCA(n_components=7,
              gamma=1,
              kernel="rbf"),
    SVC(C=10,
        gamma=1.525,
        kernel='rbf',
        class_weight=None,
        random_state=100))

svm.fit(df3.iloc[:, :-1].values, df3.iloc[:, -1].values)
df_test_sn3 = df_test.drop(["Fe", "Ba"], axis=1)
save_csv(df_test_sn3, svm,
         "compulsory_assignments/CA04/research2_scenario3.csv")

# 0.65714 on kaggle


# Research 2, scenario 2:
svm = make_pipeline(
    KernelPCA(n_components=7,
              gamma=1,
              kernel="rbf"),
    SVC(C=115,
        gamma=0.1,
        kernel='rbf',
        class_weight=None,
        random_state=100))

svm.fit(df2.iloc[:, :-1].values, df2.iloc[:, -1].values)
df_test_sn2 = df_test.drop(["RI", "Fe", "Ca"], axis=1)
save_csv(df_test_sn2, svm,
         "compulsory_assignments/CA04/research2_scenario2.csv")

# 0.65714 on kaggle

# Research 1, scenario 4:
svm = make_pipeline(
    SVC(C=100,
        gamma=1,
        kernel='rbf',
        class_weight="balanced",
        random_state=1))

svm.fit(df4.iloc[:, :-1].values, df4.iloc[:, -1].values)
save_csv(df_test, svm, "compulsory_assignments/CA04/research1_scenario4.csv")
# 0.68571 on kaggle

# Research 2, scenario 3:
svm = make_pipeline(
    SVC(C=10,
        gamma=1,
        kernel='rbf',
        class_weight=None,
        random_state=1))

svm.fit(df3.iloc[:, :-1].values, df3.iloc[:, -1].values)
df_test_sn3 = df_test.drop(["Fe", "Ba"], axis=1)
save_csv(df_test_sn3, svm,
         "compulsory_assignments/CA04/research1_scenario3.csv")

# 0.71428 on kaggle


# Research 2, scenario 2:
svm = make_pipeline(
    SVC(C=100,
        gamma=0.1,
        kernel='rbf',
        class_weight=None,
        random_state=100))

svm.fit(df2.iloc[:, :-1].values, df2.iloc[:, -1].values)
df_test_sn2 = df_test.drop(["RI", "Fe", "Ca"], axis=1)
save_csv(df_test_sn2, svm,
         "compulsory_assignments/CA04/research1_scenario2.csv")

# 0.71428 on kaggle

# =======================================================================
# Final exercise:
# =======================================================================
# Create dataframe only with float classes (0) and nonfloat classes (1):
df_float = pd.concat([df[df.type == 1], df[df.type == 3]])
df_float.type = 0
df_nonfloat = pd.concat([df[df.type == 2], df[df.type == 4]])
df_nonfloat.type = 1
df_float_nonfloat = pd.concat([df_float, df_nonfloat])
df_float_nonfloat.index = [i for i in range(1, len(df_float_nonfloat.type)+1)]

# Set up classifier:
rf = RandomForestClassifier(n_estimators=100,
                            max_depth=10,
                            class_weight="balanced",
                            n_jobs=-1,
                            random_state=100)

X = df_float_nonfloat.iloc[:, :-1].values
y = df_float_nonfloat.iloc[:, -1].values

# Cross-validation specification
cv = list(StratifiedKFold(n_splits=5).split(X, y))

fig = plt.figure(figsize=(7, 5))

mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)
all_tpr = []

# Loop through folds of CV:
for i, (train_idx, test_idx) in enumerate(cv):
    rf.fit(X[train_idx], y[train_idx])
    probas = rf.predict_proba(X[test_idx])

    # False Positive, True Positive and thresholds Rates:
    fpr, tpr, thresholds = roc_curve(y[test_idx],
                                     probas[:, 1],
                                     pos_label=1)

    # Add to mean True Predictive Rate in a smoothed variant (interpolated)
    mean_tpr += np.interp(mean_fpr, fpr, tpr)
    roc_auc = auc(fpr, tpr)

    plt.plot(fpr,
             tpr,
             label='ROC fold %d (area = %0.2f)'
                   % (i + 1, roc_auc))

plt.plot([0, 1],
         [0, 1],
         linestyle='--',
         color=(0.6, 0.6, 0.6),
         label='random guessing')

# Average True Positive Rate
mean_tpr /= len(cv)
mean_tpr[0] = 0.0
mean_tpr[-1] = 1.0

# Average AUC
mean_auc = auc(mean_fpr, mean_tpr)

plt.plot(mean_fpr, mean_tpr, 'k--',
         label='mean ROC (area = %0.2f)' % mean_auc, lw=2)

plt.plot([0, 0, 1],
         [0, 1, 1],
         linestyle=':',
         color='black',
         label='perfect performance')

plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.xlabel('false positive rate')
plt.ylabel('true positive rate')
plt.legend(loc="lower right")

plt.tight_layout()
plt.show()
