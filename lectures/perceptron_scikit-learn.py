# -*- coding: utf-8 -*-
"""

"""

# ===========================================================================
# Import modules
# ===========================================================================

import numpy as np

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score

from mlxtend.plotting import plot_decision_regions

import matplotlib.pyplot as plt

# ===========================================================================
# Load data and select features
# ===========================================================================

data = datasets.load_wine()
X = data.data[:, [0, 10]]
y = data.target

# ===========================================================================
# Split into training and test data
# ===========================================================================

# Print lables of all all classes in data set
print('Class labels:', np.unique(y))

# Split data into training and test data
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.4, random_state=5, stratify=y)

# Show distribution of classes in input data, training data and test data
# Alternative 1
print('Labels counts in y:', np.bincount(y))
print('Labels counts in y_train:', np.bincount(y_train))
print('Labels counts in y_test:', np.bincount(y_test))
print()

# Show distribution of classes in input data, training data and test data
# Alternative 2
unique, counts = np.unique(y, return_counts=True)
print('Labels counts in y:', unique, counts)

unique, counts = np.unique(y_train, return_counts=True)
print('Labels counts in y_train:', unique, counts)

unique, counts = np.unique(y_test, return_counts=True)
print('Labels counts in y_test:', unique, counts)

# ===========================================================================
# Scale features using StandardScaler class in scikit-learn
# ===========================================================================

# Initialise standard scaler and compute mean and STD from training data
sc = StandardScaler()
sc.fit(X_train)

# Transform (standardise) both X_train and X_test with mean and STD from
# training data
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

# ===========================================================================
# Train multiclass perceptron that is implemented in scikit-learn
# ===========================================================================

# Initialise the model
ppn = Perceptron(max_iter=100, eta0=0.05, random_state=77)
ppn.fit(X_train_std, y_train)

# ===========================================================================
# Make predictions for test set
# ===========================================================================

# Predict classes for samples in test set and print # of misclassfications
y_pred = ppn.predict(X_test_std)
print('Misclassified samples: {0}'.format((y_test != y_pred).sum()))

# ===========================================================================
# Compute performance metrics in a couple of ways
# ===========================================================================

# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(accuracy_score(y_test, y_pred)))

# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(ppn.score(X_test_std, y_test)))

# ===========================================================================
# Plot results with plot function for decision regions
# ===========================================================================

X_combined_std = np.vstack((X_train_std, X_test_std))
y_combined = np.hstack((y_train, y_test))

plot_decision_regions(X=X_combined_std, y=y_combined,
                      clf=ppn,
                      X_highlight=X_test_std)

plt.title('Perceptron on Wine')
plt.xlabel('{0} [standardized]'.format(data.feature_names[0]))
plt.ylabel('{0} [standardized]'.format(data.feature_names[10]))
plt.legend(loc='upper left')

plt.tight_layout()
# plt.savefig('images/03_01.png', dpi=300)
plt.show()
