# -*- coding: utf-8 -*-
"""

"""


# ==========================================================================
# Import modules
# ==========================================================================

from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# ==========================================================================
# Load data
# ==========================================================================

# Load breast cancer dataset from scikit learn
data = datasets.load_boston()


# ==========================================================================
# Inspect and visualise raw data
# ==========================================================================

# Put data into pandas dataframe to access plotting functions
data_df = pd.DataFrame(data['data'])
data_df.columns = data['feature_names']

# Plot histograms
data_df.hist()
plt.show()

# Get descriptive statistics
descr_stats = data_df.describe()
print(descr_stats)

# Make box plots to visualise distribution
data_df.plot(kind='box',
             subplots=True,
             layout=(2, 7),
             sharex=False,
             sharey=False)
plt.show()

# Box plot - all features in one plot
plt.figure(0)
data_df.boxplot()
plt.show()


# ==========================================================================
# Scale data
# ==========================================================================
X = data['data']
X_mean = np.mean(X, axis=0)
X_std = np.std(X, axis=0, ddof=0)

X_sc = (X - np.mean(X, axis=0)) / np.std(X, axis=0, ddof=0)
X_sc_mean = np.mean(X_sc, axis=0)
X_sc_std = np.std(X_sc, axis=0, ddof=0)

# Put data into pandas dataframe to access plotting functions
data_df_sc = pd.DataFrame(X_sc)
data_df_sc.columns = data['feature_names']

# Plot histograms for scaled features
data_df_sc.hist()
plt.show()

# Box plot - all scaled features in one plot
plt.figure(1)
data_df_sc.boxplot()
plt.show()

# ==========================================================================
# Splitting the data into train and test sets
# ==========================================================================
X_train = data.data[:350][:]
X_test = data.data[350:][:]

# ==========================================================================
# Scale the data, both training data and test data
# ==========================================================================

# Scale X_train and then scale X_test with parameters (mean & std) of X_train
X_train_std = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0,
                                                            ddof=0)
X_test_std = (X_test - np.mean(X_train, axis=0)) / np.std(X_train, axis=0,
                                                          ddof=0)

# Double-check mean and std of the scaled X_train
X_train_std_mean = np.mean(X_train_std, axis=0)
X_train_std_std = np.std(X_train_std, axis=0, ddof=0)

# Double-check mean and std of the scaled X_test
X_test_std_mean = np.mean(X_test_std, axis=0)
X_test_std_std = np.std(X_test_std, axis=0, ddof=0)
