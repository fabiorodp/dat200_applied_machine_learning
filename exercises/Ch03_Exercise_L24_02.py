# -*- coding: utf-8 -*-

"""
Lecture Exercise 24/02/2020 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""

__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import modules:
# ==========================================================================
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

from mlxtend.plotting import plot_decision_regions

# ===========================================================================
# Load data and select features
# ===========================================================================
bc = datasets.load_breast_cancer()
X = bc.data
y = bc.target

# ===========================================================================
# Split into training and test data
# ===========================================================================
# Split data into training and test data
train_X, test_X, train_y, test_y = \
    train_test_split(X, y, test_size=0.3, random_state=5, stratify=y)

# ===========================================================================
# Scale features using StandardScaler class in scikit-learn
# ===========================================================================
# test_X must be first here because train_X is transformed in the second
test_X = StandardScaler().fit(train_X).transform(test_X)
train_X = StandardScaler().fit(train_X).transform(train_X)

# ===========================================================================
# Train logistic regression model from scikit-learn
# ===========================================================================
lr = LogisticRegression(C=100.0, random_state=1, solver='liblinear',
                        multi_class='auto').fit(train_X, train_y)

# ===========================================================================
# Print out accuracy
# ===========================================================================
print('Accuracy: {0:.2f}'.format(lr.score(test_X, test_y)))

# ===========================================================================
# Compute class probabilities and predicted class
# ===========================================================================
# Class probabilities for the first three test classes:
classProb = lr.predict_proba(test_X)

# Predict classes for test data:
predClass = lr.predict(test_X)

# Predict a single flower sample (1 row):
predClass_oneFlower = lr.predict(test_X[0, :].reshape(1, -1))

# ===========================================================================
# Make predictions for test set
# ===========================================================================
# Predict classes and print number of misclassfications:
y_pred = lr.predict(test_X)
print('Misclassified samples: {0}'.format((test_y != y_pred).sum()))

# ===========================================================================
# Compute performance metrics in a couple of ways
# ===========================================================================
# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(accuracy_score(test_y, y_pred)))

# Print accuracy computed from predictions on the training set
print('Training data accuracy: {0:.2f}'.format(lr.score(train_X, train_y)))

# Print accuracy computed from predictions on the test set
print('Test data accuracy: {0:.2f}'.format(lr.score(test_X, test_y)))
