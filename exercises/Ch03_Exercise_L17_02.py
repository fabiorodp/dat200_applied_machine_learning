# -*- coding: utf-8 -*-

"""
Lecture Exercise 17/02/2020 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""

__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import modules:
# ==========================================================================
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score

from mlxtend.plotting import plot_decision_regions

# ==========================================================================
# Load data sets:
# ==========================================================================
data = datasets.load_wine()

# Create matrix X with all lines of the columns 0 and 10 from data:
X = data.data[:, [0, 10]]

# Create vector y with targets of [0, 10]
y = data.target

# ===========================================================================
# Split into training and test data:
# ===========================================================================
# Print lables of all all classes in data set
# print('Class labels:', np.unique(y))

# test_size := uses a percentage of the data to create a test variable
# random_state := seed
# stratify := adjust array`s dimensions between variables and targets:
train_X, test_X, train_y, test_y = \
    train_test_split(X, y, test_size=0.4, random_state=5, stratify=y)

# Show distribution of classes in input data, training data and test data
# Alternative 1
# print('Labels counts in y:', np.bincount(y))
# print('Labels counts in y_train:', np.bincount(train_y))
# print('Labels counts in y_test:', np.bincount(test_y))
# print()

# Show distribution of classes in input data, training data and test data
# Alternative 2
# unique, counts = np.unique(y, return_counts=True)
# print('Labels counts in y:', unique, counts)

# unique, counts = np.unique(train_y, return_counts=True)
# print('Labels counts in y_train:', unique, counts)

# unique, counts = np.unique(test_y, return_counts=True)
# print('Labels counts in y_test:', unique, counts)

# ===========================================================================
# Scale features using StandardScaler class in scikit-learn
# ===========================================================================
# test_X must be first here because train_X is transformed in the second
test_X = StandardScaler().fit(train_X).transform(test_X)
train_X = StandardScaler().fit(train_X).transform(train_X)

# ===========================================================================
# Train multiclass perceptron that is implemented in scikit-learn
# ===========================================================================
ppn = Perceptron(max_iter=100, eta0=0.55, random_state=77).fit(train_X,
                                                               train_y)

# ===========================================================================
# Make predictions for test set
# ===========================================================================
# Predict classes and print number of misclassfications:
y_pred = ppn.predict(test_X)
print('Misclassified samples: {0}'.format((test_y != y_pred).sum()))

# ===========================================================================
# Compute performance metrics in a couple of ways
# ===========================================================================
# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(accuracy_score(test_y, y_pred)))

# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(ppn.score(test_X, test_y)))

# ===========================================================================
# Plot results with plot function for decision regions
# ===========================================================================
combined_X = np.vstack((train_X, test_X))
combined_y = np.hstack((train_y, test_y))

plot_decision_regions(X=combined_X, y=combined_y, clf=ppn,
                      X_highlight=test_X)

plt.title('Perceptron on Wine')
plt.xlabel('{0} [standardized]'.format(data.feature_names[0]))
plt.ylabel('{0} [standardized]'.format(data.feature_names[10]))
plt.legend(loc='upper left')

plt.tight_layout()
# plt.savefig('images/03_01.png', dpi=300)
plt.show()
