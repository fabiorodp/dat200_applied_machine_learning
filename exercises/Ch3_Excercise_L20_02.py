# -*- coding: utf-8 -*-

"""
Lecture Exercise 20/02/2020 for the course 'DAT200: Applied Machine Learn' at
the University of life science (NMBU) in Ås/Norway.
"""

__author__ = 'Fábio Rodrigues Pereira'
__email__ = 'fabio.rodrigues.pereira@nmbu.no'

# ==========================================================================
# Import modules:
# ==========================================================================
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

from mlxtend.plotting import plot_decision_regions

# ===========================================================================
# Load data and select features
# ===========================================================================
iris = datasets.load_iris()
X = iris.data[:, [2, 3]]
y = iris.target

# ===========================================================================
# Split into training and test data
# ===========================================================================
# Split data into training and test data
train_X, test_X, train_y, test_y = \
    train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)

# ===========================================================================
# Scale features using StandardScaler class in scikit-learn
# ===========================================================================
# test_X must be first here because train_X is transformed in the second
test_X = StandardScaler().fit(train_X).transform(test_X)
train_X = StandardScaler().fit(train_X).transform(train_X)

# ===========================================================================
# Train logistic regression model from scikit-learn
# ===========================================================================
lr = LogisticRegression(C=100.0, random_state=1, solver='liblinear',
                        multi_class='auto').fit(train_X, train_y)

# ===========================================================================
# Plot the data
# ===========================================================================
combined_X = np.vstack((train_X, test_X))
combined_y = np.hstack((train_y, test_y))

# Specify keyword arguments to be passed to underlying plotting functions
scatter_kwargs = {'s': 120, 'edgecolor': None, 'alpha': 0.7}
contourf_kwargs = {'alpha': 0.2}
scatter_highlight_kwargs = {'s': 120, 'label': 'Test data', 'alpha': 0.7}

# Plotting decision regions
plot_decision_regions(X=combined_X,
                      y=combined_y,
                      clf=lr,
                      X_highlight=test_X,
                      scatter_kwargs=scatter_kwargs,
                      contourf_kwargs=contourf_kwargs,
                      scatter_highlight_kwargs=scatter_highlight_kwargs)

plt.xlabel('petal length [standardized]')
plt.ylabel('petal width [standardized]')
plt.legend(loc='upper left')
plt.tight_layout()
#plt.savefig('images/03_06.png', dpi=300)
plt.show()

# ===========================================================================
# Print out accuracy
# ===========================================================================
print('Accuracy: {0:.2f}'.format(lr.score(test_X, test_y)))

# ===========================================================================
# Compute class probabilities and predicted class
# ===========================================================================
# Class probabilities for the first three test classes:
classProb = lr.predict_proba(test_X)

# Predict classes for test data:
predClass = lr.predict(test_X)

# Predict a single flower sample (1 row):
predClass_oneFlower = lr.predict(test_X[0, :].reshape(1, -1))

# ===========================================================================
# Make predictions for test set
# ===========================================================================
# Predict classes and print number of misclassfications:
y_pred = lr.predict(test_X)
print('Misclassified samples: {0}'.format((test_y != y_pred).sum()))

# ===========================================================================
# Compute performance metrics in a couple of ways
# ===========================================================================
# Print accuracy computed from predictions on the test set
print('Accuracy: {0:.2f}'.format(accuracy_score(test_y, y_pred)))

# Print accuracy computed from predictions on the training set
print('Training data accuracy: {0:.2f}'.format(lr.score(train_X, train_y)))

# Print accuracy computed from predictions on the test set
print('Test data accuracy: {0:.2f}'.format(lr.score(test_X, test_y)))
